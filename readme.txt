Orx Curve Editor 0.81
=====================

This application allows you to create, load and save curve data that can be used in the Orx Portable Game Engine.

It is intended to be used to help design for any object that needs to travel along a curve, like an alien wave. Or to simply create curves for any use within a game.

Quick video guide
=================
Watch the following guide to understand how to use the editor: https://www.youtube.com/watch?v=cnwQL3WXE8s

Instructions
============
For the full readme with keyboard shortcuts, see: https://gitlab.com/sausagejohnson/orx-curvefx-editor

Credits
=======
The editor is written using Orx. 
Developed by Wayne Johnson of the Alien Abduction Unit. http://alienabductionunit.com

The Orx Portable Game Engine is developed by Iarwain. The offical site is at: http://orx-project.org