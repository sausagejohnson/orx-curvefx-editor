#ifndef CURVESEGMENT_H
#define CURVESEGMENT_H

extern orxVECTOR nullVECTOR;

class point; //forward declaration
class curvesegment
{
public:
	curvesegment(orxFLOAT pointX, orxFLOAT pointY, orxFLOAT controlX, orxFLOAT controlY);
	~curvesegment();
	void AttachNext(curvesegment *seg);
	
	void SetPointPosition(const orxVECTOR newPosition);	
	void SetControlPointPosition(const orxVECTOR newPosition);
	void RestoreColour();
	void ScalePoints(orxFLOAT zoomLevel);
	void ClearSelectedPoints();
	void PairWithSegment(curvesegment *segment);
	void DeleteOrxObjects();
	
	/* Set drawable flag and disable curve point if false */
	void SetDrawable(orxBOOL drawable);
	/* Used only on the final segment */
	void EnableSegmentCurvePoint();
	void DisableSegmentCurvePoint();

	curvesegment* CreateAdjacentCurveSegment();
	curvesegment* CreateAdjacentCurveSegmentToHead(curvesegment *headSegmentToJoinTo);
	
	orxBOOL isDrawable = orxFALSE;
	
	curvesegment *nextSegment;	
	curvesegment *pairedSegment;	

	point *curvePoint;
	point *controlPoint;
	
private:

};

#endif // CURVESEGMENT_H
