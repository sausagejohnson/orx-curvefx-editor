#include "orx.h"
#include "banner.h"

banner::banner()
{
	bannerObject = orxObject_CreateFromConfig("BannerObject");
	textObject = orxObject_CreateFromConfig("BannerTextObject");
	orxObject_Enable(textObject, orxFALSE);
	orxObject_Enable(bannerObject, orxFALSE);
}	

void banner::CreateObjects(){
	orxObject_SetLifeTime(textObject, 0);
	orxObject_SetLifeTime(bannerObject, 0);

	bannerObject = orxObject_CreateFromConfig("BannerObject");
	textObject = orxObject_CreateFromConfig("BannerTextObject");
	
	orxObject_SetParent(textObject, bannerObject);
}

void banner::DisplayBanner(orxSTRING message){
	CreateObjects();
	orxObject_SetTextString(textObject, message);
}

banner::~banner()
{
}

