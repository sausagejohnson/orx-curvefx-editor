#include "orx.h"
#include "editor.h"

#ifdef __orxMSVC__

/* Requesting high performance dedicated GPU on hybrid laptops */
__declspec(dllexport) unsigned long NvOptimusEnablement        = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

#endif // __orxMSVC__

void SetupColours(){
	orxVECTOR red = {1, 0, 1};
	orxVECTOR white = {1, 1, 1};
	orxVECTOR blue = {0, 0, 1};

	curveColour.u8R = 255;
	curveColour.u8G = 128;
	curveColour.u8B = 0;
	curveColour.u8A = 255;
	
	controlLineColour.u8R = 255;
	controlLineColour.u8G = 255;
	controlLineColour.u8B = 255;
	controlLineColour.u8A = 255;
	
		
	gridColour.u8R = 90;
	gridColour.u8G = 90;
	gridColour.u8B = 90;
	gridColour.u8A = 255;
	
	gridXZeroColour.u8R = 0;
	gridXZeroColour.u8G = 200;
	gridXZeroColour.u8B = 0;
	gridXZeroColour.u8A = 255;
	
	gridYZeroColour.u8R = 255;
	gridYZeroColour.u8G = 0;
	gridYZeroColour.u8B = 0;
	gridYZeroColour.u8A = 255;
	

}

orxBOOL OrxStringEqual(const orxSTRING one, const orxSTRING two){
	orxS32 compare = orxString_Compare(one, two);
	if (compare == 0){
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL orxFASTCALL SaveCurveToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// Filters out everything but the "Curve" and "Background" sections.
	// And only the "Texture" key for the "Background" section.
	return (
		orxString_Compare(_zSectionName, "Curve") == 0 ||
			(
				orxString_Compare(_zSectionName, "Background") == 0 && 
				(_zKeyName == orxNULL || orxString_Compare(_zKeyName, "Texture") == 0)
			)
		) ? orxTRUE : orxFALSE;
}

orxBOOL orxFASTCALL SaveSettingsToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// Return orxTRUE for the section "Curve", orxFALSE otherwise
	// -> filters out everything but the "Curve" section
	return (
		orxString_Compare(_zSectionName, "Curve") == 0 ||
		orxString_Compare(_zSectionName, "EditorSettings") == 0
		) ? orxTRUE : orxFALSE;
}


void ToggleHelp(){
	orxObject_EnableRecursive(helpObject, !orxObject_IsEnabled(helpObject));
}

curvesegment* GetFinalSegment(){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		if (segment->nextSegment == NULL){
			return segment;
		} 
		segment = segment->nextSegment;
	}
	return NULL;
}



int GetDrawableSegmentsInCurveCount(){
	curvesegment *segment = rootCurveSeg;
	int count = 0;
	while (segment != NULL){
		if (segment->isDrawable){
			count++;
		}
		segment = segment->nextSegment;
	}
	return count;
}


int GetSegmentsInCurveCount(){
	curvesegment *segment = rootCurveSeg;
	int count = 0;
	while (segment != NULL){
		count++;
		segment = segment->nextSegment;
	}
	return count;
}




void CorrectLastSegment(){
	curvesegment *segment = GetFinalSegment();
	segment->EnableSegmentCurvePoint();
}

orxBOOL OpenQuitDialog(){
	orxBOOL choice = tinyfd_messageBox ( 
		"Quit?", 
		"Are you sure you want to quit?",
		"yesno",
		"question",
		0
	);
	
	return choice;
}

orxBOOL OpenSaveConfirmDialog(){
	orxCHAR formattedMessage[255];
	orxString_NPrint(formattedMessage, 255, "%s already exists.\nDo you wish to overwrite it?", 
					lastSavePath); 
	
	orxBOOL choice = tinyfd_messageBox ( 
		"Save File", 
		formattedMessage,
		"okcancel",
		"question",
		1
	);
	
	return choice;
}

const orxSTRING SaveFileDialog(){
	const orxSTRING saveFileName;
	const int numberOfPatterns = 3;
	const orxSTRING filterPatterns[numberOfPatterns] = { "*.ini", "*.config", "*.cfg" };
	
	saveFileName = tinyfd_saveFileDialog (
		"Save curve to file",
		"", 
		numberOfPatterns, 
		filterPatterns, 
		"Orx config data file (*.ini, *.config, *.cfg)"
	);
	
	//orxLOG("Path: %s \n", saveFileName);
	return saveFileName;
}


const orxSTRING OpenFileDialog(){

	const orxSTRING openFileName;
	const int numberOfPatterns = 3;
	const orxSTRING filterPatterns[numberOfPatterns] = { "*.ini", "*.config", "*.cfg" };
	const orxBOOL allowMultipleSelects = orxFALSE;
	const int allowMultipleSelectsUTF16 = 0;

	tinyfd_winUtf8 = 1;

	openFileName = tinyfd_openFileDialog(
		"Select an Orx .ini file containing curve data.",
		"",
		numberOfPatterns,
		filterPatterns,
		"Orx config data file (*.ini, *.config, *.cfg)",
		allowMultipleSelects);

	//orxLOG("Path: %s \n", openFileName);

	return openFileName;
}



const orxSTRING OpenImageFileDialog(){

	const orxSTRING openFileName;
	const int numberOfPatterns = 2;
	const orxSTRING filterPatterns[numberOfPatterns] = { "*.png", "*.jpg" };
	const orxBOOL allowMultipleSelects = orxFALSE;

	openFileName = tinyfd_openFileDialog(
		"Select an image file format.",
		"",
		numberOfPatterns,
		filterPatterns,
		"Image file (*.png, *.jpg)",
		allowMultipleSelects);

	//orxLOG("Path: %s \n", openFileName);

	return openFileName;
}


void ClearEntireCurve(){
	orxClock_Pause(pstClock, orxTRUE);
	
	curvesegment *segment = rootCurveSeg;
	curvesegment *parentSegment;

	int count = 0;
	while (segment != NULL){
		count++;
		parentSegment = segment;
		segment = segment->nextSegment;
		
		parentSegment->DeleteOrxObjects();
		delete parentSegment;
	}
}

curvesegment* GetParentSegment(curvesegment *childSegment){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		if (segment->nextSegment == childSegment){
			return segment;
		} 
		segment = segment->nextSegment;
	}
	return NULL;
}

/* Make curve if data is in the ini memory*/
void CreateCurve(){
	int pointCount = 0;

	if(orxConfig_PushSection("Curve")) {
		ClearEntireCurve();
		
		pointCount = orxConfig_GetListCount("Points");
		
		orxVECTOR* vectorArray = NULL;
		vectorArray = new orxVECTOR[pointCount];
		
		for (int x=0; x<pointCount; x++){
			vectorArray[x] = orxVECTOR_0;
			orxConfig_GetListVector("Points", x, &vectorArray[x]);
		}
		
		curvesegment *parentSegment;
		
		for (int x=0; x<pointCount; x+=2){
			if (x == 0){
				curvesegment *segment = new curvesegment(vectorArray[x].fX, vectorArray[x].fY, vectorArray[x+1].fX, vectorArray[x+1].fY);
				rootCurveSeg = segment;
				parentSegment = rootCurveSeg;
			}
			if (x == 2){
				curvesegment *segment = new curvesegment(vectorArray[x+1].fX, vectorArray[x+1].fY, vectorArray[x].fX, vectorArray[x].fY);
				segment->SetDrawable(orxFALSE);
				parentSegment->nextSegment = segment;
				parentSegment = segment;
			}
			if (x > 2){
				if ((x % 4) == 0 ){
					curvesegment *segment = new curvesegment(vectorArray[x].fX, vectorArray[x].fY, vectorArray[x+1].fX, vectorArray[x+1].fY);
					
					segment->pairedSegment = parentSegment;
					parentSegment->pairedSegment = segment;
					parentSegment->nextSegment = segment;
					parentSegment = segment;
				} else {
					curvesegment *segment = new curvesegment(vectorArray[x+1].fX, vectorArray[x+1].fY, vectorArray[x].fX, vectorArray[x].fY);

					parentSegment->nextSegment = segment;
					segment->SetDrawable(orxFALSE);		
					parentSegment = segment;
				}
			}
		}
		
		curvesegment *lastSegment = GetFinalSegment();
		lastSegment->EnableSegmentCurvePoint();

		orxConfig_PopSection();
	} 

}

void MoveEntireCurveByDeltaOffset(orxVECTOR delta){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		orxVECTOR position = segment->curvePoint->GetPosition();
		orxVector_Add(&position, &position, &delta);
		segment->curvePoint->SetPosition(position);
		
		position = segment->controlPoint->GetPosition();
		orxVector_Add(&position, &position, &delta);
		segment->controlPoint->SetPosition(position);

		segment = segment->nextSegment;
	}
}

orxVECTOR GetTopLeftVectorOfTheCurve(){
	orxVECTOR topLeftVector = orxVECTOR_0;
	topLeftVector.fX = rootCurveSeg->curvePoint->GetPosition().fX;
	topLeftVector.fY = rootCurveSeg->curvePoint->GetPosition().fY;

	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		orxVECTOR position = segment->curvePoint->GetPosition();
		if (position.fX < topLeftVector.fX){
			topLeftVector.fX = position.fX;
		}
		if (position.fY < topLeftVector.fY){
			topLeftVector.fY = position.fY;
		}
		position = segment->controlPoint->GetPosition();
		if (position.fX < topLeftVector.fX){
			topLeftVector.fX = position.fX;
		}
		if (position.fY < topLeftVector.fY){
			topLeftVector.fY = position.fY;
		}
		segment = segment->nextSegment;
	}
	
	return topLeftVector;
}

orxVECTOR GetBottomRightVectorOfTheCurve(){
	orxVECTOR bottomRightVector = orxVECTOR_0;
	bottomRightVector.fX = rootCurveSeg->curvePoint->GetPosition().fX;
	bottomRightVector.fY = rootCurveSeg->curvePoint->GetPosition().fY;

	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		orxVECTOR position = segment->curvePoint->GetPosition();
		if (position.fX > bottomRightVector.fX){
			bottomRightVector.fX = position.fX;
		}
		if (position.fY > bottomRightVector.fY){
			bottomRightVector.fY = position.fY;
		}
		position = segment->controlPoint->GetPosition();
		if (position.fX > bottomRightVector.fX){
			bottomRightVector.fX = position.fX;
		}
		if (position.fY > bottomRightVector.fY){
			bottomRightVector.fY = position.fY;
		}
		segment = segment->nextSegment;
	}
	
	return bottomRightVector;
}

orxVECTOR GetCurveSize(orxVECTOR topLeft, orxVECTOR bottomRight){
	orxVECTOR size = orxVECTOR_0;
	orxVector_Sub(&size, &bottomRight, &topLeft);
	return size;
}

void FlipOnX(){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		orxVECTOR position = segment->curvePoint->GetPosition();
		position.fX += (-position.fX *2); 
		segment->curvePoint->SetPosition(position);
		
		position = segment->controlPoint->GetPosition();
		position.fX += (-position.fX *2); 
		segment->controlPoint->SetPosition(position);

		segment = segment->nextSegment;
	}
}

void FlipOnY(){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		orxVECTOR position = segment->curvePoint->GetPosition();
		position.fY += (-position.fY *2); 
		segment->curvePoint->SetPosition(position);
		
		position = segment->controlPoint->GetPosition();
		position.fY += (-position.fY *2); 
		segment->controlPoint->SetPosition(position);

		segment = segment->nextSegment;
	}
}

void ScaleCurvePoints(){
	orxFLOAT pointsZoom = zoomLevel;
	if (pointsZoom > 0){
		pointsZoom = 1 / pointsZoom;
	}
	if (pointsZoom < 0){
		pointsZoom = 1 / pointsZoom;
	}
	rootCurveSeg->ScalePoints(pointsZoom);
}

void LoadAndBuildCurveFromConfig(){
	
	
	orxSTATUS loadStatus = orxSTATUS_SUCCESS;
	
	const orxSTRING zLoadFile = "settings.ini"; 
	loadStatus = orxConfig_Load(zLoadFile);
	
	if (loadStatus == orxSTATUS_FAILURE){
		return;
	}
	
	CreateCurve();
}

orxSTRING ConvertCurveToCharArray(){
	int segmentCount = GetSegmentsInCurveCount();
	int pointCount = segmentCount * 2;
	orxBOOL flip = orxFALSE;
	
	orxVECTOR* vectorArray = NULL;
	vectorArray = new orxVECTOR[pointCount];
	
	for (int x=0; x<pointCount; x++){
		vectorArray[x] = orxVECTOR_0;
	}
	
	int index = 0;
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){
		if (segment->isDrawable == orxTRUE){
			vectorArray[index] = segment->curvePoint->GetPosition();
			index++;
			vectorArray[index] = segment->controlPoint->GetPosition();
			index++;
		} else {
			vectorArray[index] = segment->controlPoint->GetPosition();
			index++;
			vectorArray[index] = segment->curvePoint->GetPosition();
			index++;
		}
		flip = !flip;
		segment = segment->nextSegment;
	}
	

	static orxCHAR acBuffer[32768] = {}; 
	orxU32  u32Offset = 0; 

	// For all vectors 
	for(int i = 0; i < pointCount; i += 1) 
	{ 
		orxVECTOR v = vectorArray[i]; 

		u32Offset += orxString_NPrint(acBuffer + u32Offset, sizeof(acBuffer) - 1 - u32Offset, "%s(%g, %g, %g)", (i != 0) ? " # " : orxSTRING_EMPTY, v.fX, v.fY, v.fZ); 
	} 
	
	delete [] vectorArray;
	
	//orxSTRING stringCurve = acBuffer;
	return acBuffer;
	//return stringCurve;
}


void SaveSettingsToFile(){
	
	orxSTRING acBuffer;
	acBuffer = ConvertCurveToCharArray(); 
	
	orxSTATUS saveStatus = orxSTATUS_SUCCESS;
	
	if (orxConfig_PushSection("Curve")) {
		orxConfig_SetString("Points", acBuffer); 
		orxConfig_PopSection();
	}
	
	orxVECTOR cameraPosition = orxVECTOR_0;
	orxCamera_GetPosition(camera, &cameraPosition);
	
	if(orxConfig_PushSection("EditorSettings")) {
		orxConfig_SetFloat("Speed", moveAlongSpeed); 	
		orxConfig_SetFloat("Spread", moveAlongSpread); 			
		orxConfig_SetFloat("Zoom", zoomLevel); 
		orxConfig_SetVector("ScreenPosition", &cameraPosition); 
/*		orxConfig_SetBool("CurveObjectPaused", moveAlongPaused);
		orxConfig_SetBool("CurveObjectEnabled", !orxClock_IsPaused(pstClock));*/

		orxConfig_PopSection();
	}

	const orxSTRING zSaveFile = "settings.ini"; 
	orxConfig_Save(zSaveFile, orxFALSE, SaveSettingsToConfigFileCallback);

}

void ClearBackground(){
	if (orxConfig_PushSection("Background")) {
		orxConfig_SetString("Texture", "pixel"); 
		orxConfig_PopSection();
	}
	
	SetBackground("");
}

void LoadCurveFromFile(){
	//are objects moving before loading? Then enable them after
	orxBOOL objectsEnabledBeforeLoad = !orxClock_IsPaused(pstClock);
	
	orxSTRING path = (orxCHAR*)OpenFileDialog();
	if (path == NULL){
		notification->DisplayBanner("load curve\ncancelled");
		return;
	} else {
		lastSavePath = path;
		
		//clear out any background before loading.
		ClearBackground();
	}
	//orxLOG("File: %s", path);
	
	orxSTATUS loadStatus = orxSTATUS_SUCCESS;
	
	const orxSTRING zLoadFile = path; 
	loadStatus = orxConfig_Load(zLoadFile);
	
	if (loadStatus == orxSTATUS_FAILURE){
		notification->DisplayBanner("load curve\n failed");
		return;
	}
	
	CreateCurve();
	InitialiseMoveAlongIndexesAndSegments();
	ScaleCurvePoints();
	ScaleDragCursor();
	
	EnableCurveObjects(objectsEnabledBeforeLoad);
	
	SetBackground("");
}

void OpenBackgroundFromFile(){

	orxSTRING path = (orxCHAR*)OpenImageFileDialog();
	if (path == NULL){
		notification->DisplayBanner("load background\ncancelled");
		return;
	} 
	
	SetBackground(path);

}

void SetBackground(orxSTRING path){
	if (OrxStringEqual(path, "") == orxFALSE){ //was changed via load already? Don't set anything
		if (orxConfig_PushSection("Background")) {
			orxConfig_SetString("Texture", path); 
			orxConfig_PopSection();
		}
	}
	
	orxObject_SetLifeTime(background, 0);
	background = orxObject_CreateFromConfig("Background");
}

void EnableMoveAlongs(orxBOOL enable){
	for (int x=0; x<8; x++){
		orxObject_Enable(objectsAlongCurve[x], enable);
	}
}


void ToggleCurveObjects(){
	//orxLOG("pause:");
	if (orxClock_IsPaused(pstClock) ){ //Then we'll enable the objects
		moveAlongSegment = rootCurveSeg;
		moveAlongPositionIndex = 0;
		moveAlongPaused = orxFALSE;
		orxClock_Pause(pstClock, orxFALSE);
		EnableMoveAlongs(!moveAlongPaused);
		notification->DisplayBanner("curve object\n    on");
	} else { // then we'll disable the objects
		moveAlongPaused = orxTRUE;
		orxClock_Pause(pstClock, orxTRUE);
		EnableMoveAlongs(!moveAlongPaused);
		notification->DisplayBanner("curve object\n    off");
	}
}

void EnableCurveObjects(orxBOOL enable){
	if (enable == orxTRUE){
		moveAlongSegment = rootCurveSeg;
		moveAlongPositionIndex = 0;
		moveAlongPaused = orxFALSE;
		EnableMoveAlongs(!moveAlongPaused);
		orxClock_Pause(pstClock, orxFALSE);
		//notification->DisplayBanner("curve object\n    on");
	} else { // then we'll disable the objects
		moveAlongPaused = orxTRUE;
		EnableMoveAlongs(!moveAlongPaused);
		orxClock_Pause(pstClock, orxTRUE);
		//notification->DisplayBanner("curve object\n    off");
	}
}

void LoadSettingsFromFile(){
	orxSTATUS loadStatus = orxSTATUS_SUCCESS;
	
	const orxSTRING zLoadFile = "settings.ini"; 
	loadStatus = orxConfig_Load(zLoadFile);
	
	if (loadStatus == orxSTATUS_FAILURE){
		return;
	}
	
	orxVECTOR cameraPosition = orxVECTOR_0;
	
	if (orxConfig_PushSection("EditorSettings")) {
		if (orxConfig_HasValue("Speed")){
			moveAlongSpeed = orxConfig_GetFloat("Speed");
		}
		if (orxConfig_HasValue("Spread")){
			moveAlongSpread = orxConfig_GetFloat("Spread");
		}
		if (orxConfig_HasValue("Zoom")){
			zoomLevel = orxConfig_GetFloat("Zoom"); 
			orxCamera_SetZoom(camera, zoomLevel);
		}
		if (orxConfig_HasValue("ScreenPosition")){
			orxConfig_GetVector("ScreenPosition", &cameraPosition); 
			orxCamera_SetPosition(camera, &cameraPosition);
		}		 		 
		
		orxConfig_PopSection();
	}
	
	ScaleCurvePoints();
}

void SaveCurveToConfigFile(){
	orxBOOL exists = orxFALSE;
	if (lastSavePath != orxNULL && !OrxStringEqual(lastSavePath, "")){
		exists = orxFile_Exists(lastSavePath);
	}
 
	if (exists == orxTRUE){
		orxBOOL okToOverwrite = OpenSaveConfirmDialog();
		if (!okToOverwrite){
			notification->DisplayBanner("save curve\ncancelled");
			return;
		}
	}

	//orxLOG("File: %s", lastSavePath);
	
	orxSTRING acBuffer;
	acBuffer = ConvertCurveToCharArray(); 

	orxSTATUS saveStatus = orxSTATUS_SUCCESS;
	
	if(orxConfig_PushSection("Curve")) {
		orxConfig_SetString("Points", acBuffer); 
		orxConfig_PopSection();

		//const orxSTRING zSaveFile = orxFile_GetApplicationSaveDirectory("curvesoutput.ini"); 
		const orxSTRING zSaveFile = lastSavePath;
		saveStatus = orxConfig_Save(zSaveFile, orxFALSE, SaveCurveToConfigFileCallback);
	}

	if (saveStatus == orxSTATUS_SUCCESS){
		notification->DisplayBanner("curve saved");
	} else {
		notification->DisplayBanner("error saving\n\ncurve data");
	}
}


void SaveCurveToFile(){
	
	if (lastSavePath == orxNULL || OrxStringEqual(lastSavePath, "")){
		orxSTRING path = (orxCHAR*)SaveFileDialog();
	
		if (path == NULL){
			notification->DisplayBanner("save curve\ncancelled");
		} else {
			lastSavePath = path;
			SaveCurveToConfigFile();
		}
	} else {
		SaveCurveToConfigFile();
	}
	
}

void SaveAsCurveToFile(){
	
	orxSTRING path = (orxCHAR*)SaveFileDialog();
	
	if (path == NULL){
		notification->DisplayBanner("save curve\ncancelled");
	} else {
		lastSavePath = orxString_Duplicate(path); //potentially dangerous????
		SaveCurveToFile();
	}
}


point* GetHighlightedPoint(){
	curvesegment *segment = rootCurveSeg;
	while (segment != NULL){

	
		point *p = segment->curvePoint;
		if (p->isSelected){
			return p;
		}
		
		segment = segment->nextSegment;	
	}
	return NULL;
}

void DrawGrid(){
	
	orxVECTOR lineStart = orxVECTOR_0;
	orxVECTOR lineEnd = orxVECTOR_0;
	orxFLOAT z = 0;

	for (int x=-1000; x<2000; x+=gridDistance){
		lineStart = {(orxFLOAT)x, -1000, z};
		lineEnd = {(orxFLOAT)x, 2000, z};

		orxRender_GetScreenPosition(&lineStart, orxNULL, &lineStart);
		orxRender_GetScreenPosition(&lineEnd, orxNULL, &lineEnd);

		orxDisplay_DrawLine ( &lineStart, &lineEnd, gridColour);
		
		if (x == 0){
			orxDisplay_DrawLine ( &lineStart, &lineEnd, gridXZeroColour);
		}
	}
	
	for (int y=-1000; y<2000; y+=gridDistance){
		lineStart = {-1000, (orxFLOAT)y, z};
		lineEnd = {2000, (orxFLOAT)y, z};

		orxRender_GetScreenPosition(&lineStart, orxNULL, &lineStart);
		orxRender_GetScreenPosition(&lineEnd, orxNULL, &lineEnd);

		orxDisplay_DrawLine ( &lineStart, &lineEnd, gridColour);
		
		if (y == 0){
			orxDisplay_DrawLine ( &lineStart, &lineEnd, gridYZeroColour);
		}
	}
}

orxFLOAT GetRotationFromVectors(orxVECTOR vectorTwo, orxVECTOR vectorOne){					
	orxFLOAT tan = orxMath_ATan( (vectorOne.fX-vectorTwo.fX), (vectorOne.fY-vectorTwo.fY) );
	return -tan;
}


/* Pass in 3 to get the third drawable segment from the root linked in the list 
 * 0 would return the root
 */
curvesegment* GetDrawableSegmentInList(int index){
	curvesegment *segment = rootCurveSeg;
	for (int x=0; x<index; x++){
		segment = segment->nextSegment;
		if (segment != NULL && !segment->isDrawable){
			segment = segment->nextSegment;
		}

		if (segment == NULL){
			return NULL;
		}
	}
	if (segment != NULL){
		return segment;
	}
	return NULL;
}

/* Pass in 3 to get the third segment from the root linked in the list 
 * 0 would return the root
 */
curvesegment* GetSegmentInList(int index){
	curvesegment *segment = rootCurveSeg;
	for (int x=0; x<index; x++){
		segment = segment->nextSegment;
		if (segment == NULL){
			return NULL;
		}
	}
	if (segment != NULL){
		return segment;
	}
	return NULL;
}


//eg 0.5, 0.525, 0.6 to get 0.25 (between 0 and 1)
orxDOUBLE NormalisedValueFromRangeAndValue(orxDOUBLE min, orxDOUBLE val, orxDOUBLE max){ //2  2.5  3
	orxDOUBLE shiftToZero = min;	//2
	orxDOUBLE normalisedMin = min - shiftToZero; 		//2-2 = 0
	orxDOUBLE normalisedMax = (max - shiftToZero) / (max - shiftToZero);   	//(3-2) / (3-2) = 1
	orxDOUBLE normalisedValue = (val - shiftToZero) / (max - shiftToZero);  //(2.5-2) / (3-2) = (0.5) / 1 = 0.5

	return normalisedValue;

}


/* Enter a nomalisedValue between 0 and 1
 * This will take all the segments end to end and
 * determine which segment this nomalisedValue falls in,
 * then which exact point in that segment, 
 * then what the vector is, eg:
 *   seg0   seg1   seg2   seg3   seg4
 * |------|------|------|------|------|
 * |0---------------^0.5-------------1|
 * 0.5 will be half way through segment 3
 * EG 5 segments, and 0.5 passed in:
 * 5/5 = 1;   
 * 			  0.1*5 = 0.5 (int) 0 segment
 *            0.2*5 = 1 segment
 * 			  0.4*5 = 2 segment
 *            0.5*5 = 2.5 (int) 2 segment
 *            0.6*5 = 3 segment
 *            0.8*5 = 4 segment
 *            0.9*5 = 4.5 (int) 5 segment
 */
orxVECTOR GetPositionFromMoveAlongIndex(orxDOUBLE nomalisedValue){ // 0 to 1
	int drawableSegmentCount = GetDrawableSegmentsInCurveCount();
	
	int index = nomalisedValue * drawableSegmentCount;
	orxDOUBLE valueInRange = nomalisedValue * drawableSegmentCount;
	
	curvesegment *segment = GetDrawableSegmentInList(index);
	
	orxDOUBLE normalisedInsideSegment = NormalisedValueFromRangeAndValue((orxDOUBLE)index, valueInRange, (orxDOUBLE)index+1);
	
	orxVECTOR vector = orxVECTOR_0;
	
	if (segment->nextSegment != NULL){
		orxVECTOR p1 = segment->curvePoint->GetScreenPosition();
		orxVECTOR cp1 = segment->controlPoint->GetScreenPosition();
		orxVECTOR cp2 = segment->nextSegment->controlPoint->GetScreenPosition();
		orxVECTOR p2 = segment->nextSegment->curvePoint->GetScreenPosition();

		orxVector_Bezier (&vector,
							&p1,
							&cp1,
							&cp2,
							&p2,
							normalisedInsideSegment
		);	
	}
	
	return vector;
}


void MoveAlongObjects(){
	for (int x=0; x<8; x++){
		orxVECTOR movePoint = GetPositionFromMoveAlongIndex(moveAlongPositionIndexes[x]);
	
		orxVECTOR localPosition = movePoint;
		orxRender_GetWorldPosition(&localPosition, orxNULL, &localPosition);
		localPosition.fZ = 0;
		orxObject_SetWorldPosition(objectsAlongCurve[x], &localPosition);

		orxFLOAT moveAlongToPositionIndex = moveAlongPositionIndexes[x]+moveAlongSpeed;
		
		if (moveAlongToPositionIndex < 1){
			orxVECTOR moveToPoint = GetPositionFromMoveAlongIndex(moveAlongToPositionIndex);

			orxFLOAT rotation = 0;
			rotation = GetRotationFromVectors(moveToPoint, movePoint);
			orxObject_SetRotation(objectsAlongCurve[x], rotation);
				
		}
	}
	

}


/*
 * Reset the move alongs. Hide all except the leader.
 * Used when resetting the spread distance
 * */
void ResetMoveAlongsToZero(){
	for (int x=0; x<8; x++){
		moveAlongPositionIndexes[x] = 0;
		if ( x==0 ){
			orxObject_Enable(objectsAlongCurve[x], orxTRUE);
		} else {
			orxObject_Enable(objectsAlongCurve[x], orxFALSE);
		}
	}
}

void orxFASTCALL MoveAlongUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pContext){

	MoveAlongObjects();	
	
	if (!moveAlongPaused){
		for (int x=7; x>=0; x--){
			if (moveAlongPositionIndexes[x] > 0 && moveAlongPositionIndexes[x] < 1){
				orxObject_Enable(objectsAlongCurve[x], orxTRUE);
			} else {
				orxObject_Enable(objectsAlongCurve[x], orxFALSE);
			}
			
			int nextPositionIndex = x-1;

			if (x == 0){
				moveAlongPositionIndexes[x] += moveAlongSpeed;
				if (moveAlongPositionIndexes[x] >= 1){
					moveAlongPositionIndexes[x] = 0;
				}
			} else {
				if (moveAlongPositionIndexes[x] < (moveAlongPositionIndexes[nextPositionIndex] - moveAlongSpread) || 
					moveAlongPositionIndexes[nextPositionIndex] < moveAlongPositionIndexes[x]
				){
					moveAlongPositionIndexes[x] += moveAlongSpeed;
				}
				if (moveAlongPositionIndexes[x] >= 1){
					moveAlongPositionIndexes[x] = 0;
					if (moveAlongSegments[x]->nextSegment == NULL || moveAlongSegments[x]->nextSegment->pairedSegment == NULL){
						moveAlongSegments[x] = moveAlongSegments[x]->nextSegment->pairedSegment;
					} else {
						moveAlongSegments[x] = rootCurveSeg;
					}
					
					if (moveAlongSegments[x] == NULL){
						moveAlongSegments[x] = rootCurveSeg;
					}
				}
			}
		
			
			
		}

	}
}

void DrawGrabBox(){
	orxVECTOR topLeft = GetTopLeftVectorOfTheCurve();
	orxRender_GetScreenPosition(&topLeft, orxNULL, &topLeft);
	orxVECTOR bottomRight = GetBottomRightVectorOfTheCurve();
	orxRender_GetScreenPosition(&bottomRight, orxNULL, &bottomRight);
	orxVECTOR curveSize = GetCurveSize(topLeft, bottomRight);
	//orxRender_GetScreenPosition(&curveSize, orxNULL, &curveSize);
	//int here = 1;

	orxOBOX boxArea;

	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;

	orxOBox_2DSet(&boxArea, &topLeft, &pivot, &curveSize, 0);

	orxRGBA colour;
	colour.u8R = 0;
	colour.u8G = 255;
	colour.u8B = 128;
	colour.u8A = 50;
	orxDisplay_DrawOBox( &boxArea, colour, orxTRUE);	

}

void DrawSegments(){
	
	orxFLOAT steps = 0.01;
	
	orxVECTOR drawPoint = nullVECTOR;
	
	curvesegment *segment = rootCurveSeg;

	while (segment != NULL){
		if (segment->nextSegment == NULL){
			break;
		} 
		if (segment->isDrawable == orxFALSE){
			segment = segment->nextSegment;	
			continue;
		}
		
		orxVECTOR p1 = segment->curvePoint->GetScreenPosition();
		orxVECTOR cp1 = segment->controlPoint->GetScreenPosition();
		orxVECTOR cp2 = segment->nextSegment->controlPoint->GetScreenPosition();
		orxVECTOR p2 = segment->nextSegment->curvePoint->GetScreenPosition();
		
		orxDisplay_DrawLine ( &p1, &cp1, controlLineColour);
		orxDisplay_DrawLine ( &p2, &cp2, controlLineColour);

		for (float x=0; x<1; x+=steps){
			if (x+steps <= 1){
				
				orxVector_Bezier (&drawPoint,
								&p1,
								&cp1,
								&cp2,
								&p2,
								x
				);	
				
				const orxVECTOR vv = drawPoint1;
				
				if (orxVector_AreEqual(&vv, &nullVECTOR) == orxTRUE){
					drawPoint.fZ = 0;
					drawPoint1 = drawPoint;
					//return; //fill and exit first time. Don't draw the fragment
				}
				
				drawPoint.fZ = 0;
				drawPoint2 = drawPoint;
			
				orxDisplay_DrawLine ( &drawPoint1, &drawPoint2, curveColour);
				
				//Get ready for the next line fragment of the curve to draw
				drawPoint1 = drawPoint2;
				drawPoint2 = nullVECTOR;
			}
		}
		
		//Clear out all to restart drawing the curve
		drawPoint1 = nullVECTOR;
		drawPoint2 = nullVECTOR;
		
		segment = segment->nextSegment;	

	}


}


/* This changes the frustum slightly to show a little decorative edge if the device shows black bars on aspect. */
void SetFrustumToWhateverDisplaySizeCurrentlyIs(){
	
	orxFLOAT deviceWidth = 1024; //default
	orxFLOAT deviceHeight = 768; //default
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE){
		orxCamera_SetFrustum(camera, deviceWidth, deviceHeight, 0, 2);
	}
}

/* Ensure the bar is always the width of the screen. */
void SetBarToWhateverDisplayWidthCurrentlyIs(){
	
	orxFLOAT deviceWidth = 1024; //default
	orxFLOAT deviceHeight = 768; //unused
	
	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE){
		orxVECTOR vector = orxVECTOR_0;
		
		/* Force header to top left of the display */
		orxObject_GetPosition(headerObject, &vector);
		vector.fX = -deviceWidth/2;
		vector.fY = -deviceHeight/2;
		orxObject_SetPosition(headerObject, &vector);

		for (orxOBJECT *pstChild = orxObject_GetOwnedChild(headerObject); 
			pstChild;
			pstChild = orxObject_GetOwnedSibling(pstChild))
			{
				const orxSTRING name = orxObject_GetName(pstChild);
				if (orxString_Compare(name, "BarObject") == 0) {
					orxObject_GetScale(pstChild, &vector);
					vector.fX = deviceWidth;
					orxObject_SetScale(pstChild, &vector);
					
				}
				if (orxString_Compare(name, "PointPositionTextObject") == 0) {
					orxObject_GetPosition(pstChild, &vector);
					vector.fX = deviceWidth - 30;
					orxObject_SetPosition(pstChild, &vector);
				}
				
				
			}
	}
}

orxSTATUS orxFASTCALL ViewportEventHandler(const orxEVENT *_pstEvent) {
		
	if (_pstEvent->eID == orxVIEWPORT_EVENT_RESIZE){
		SetFrustumToWhateverDisplaySizeCurrentlyIs();
		SetBarToWhateverDisplayWidthCurrentlyIs();
	}
	
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL RenderEventHandler(const orxEVENT *_pstEvent) {
	if (_pstEvent->eType == orxEVENT_TYPE_RENDER){
		if(_pstEvent->eID == orxRENDER_EVENT_OBJECT_START){ //orxRENDER_EVENT_STOP){// || _pstEvent->eID == orxRENDER_EVENT_START){
			orxRENDER_EVENT_PAYLOAD *pstPayload;
			//pstPayload = (orxRENDER_EVENT_PAYLOAD *)_pstEvent->pstPayload; 
			
			orxOBJECT *recipient = orxOBJECT(_pstEvent->hRecipient);
			const orxSTRING recipientName = orxObject_GetName(recipient);
			
			if (OrxStringEqual(recipientName, "CurveLayerObject")){
				DrawGrid();
				DrawSegments();
				DrawSegments(); //oddly smoother when I run this routine a second time.
				if (grabMode == orxTRUE){
					DrawGrabBox();
				}
			}

		}
	}
	
	return orxSTATUS_SUCCESS;
	
}

void SnapPoint(point *pointToSnap){
		if (pointToSnap != NULL){
			orxVECTOR difference = orxVECTOR_0;
			orxVECTOR controlDifference = orxVECTOR_0;
			
			orxVECTOR position = pointToSnap->GetPosition();
			orxVECTOR originalPosition = position;
			
			position.fX = orxMath_Round(position.fX / gridDistance) * gridDistance;
			position.fY = orxMath_Round(position.fY / gridDistance) * gridDistance;
			
			pointToSnap->SetPosition(position);
			
			orxVector_Sub(&difference, &position, &originalPosition);
			controlDifference = pointToSnap->owningSegment->controlPoint->GetPosition();
			orxVector_Add(&controlDifference, &controlDifference, &difference);
			pointToSnap->owningSegment->controlPoint->SetPosition(controlDifference);
			
			/* paired segment point must match */
			curvesegment *ownerOfPoint = pointToSnap->owningSegment;
			if (ownerOfPoint->pairedSegment != NULL){
				point *pairedPoint = ownerOfPoint->pairedSegment->curvePoint;
				pairedPoint->SetPosition(position);
			}
			
			/* also move paired segment control point if there is one */
			if (ownerOfPoint->pairedSegment != NULL){
				point *pairedControlPoint = ownerOfPoint->pairedSegment->controlPoint;
				controlDifference = pairedControlPoint->GetPosition();
				orxVector_Add(&controlDifference, &controlDifference, &difference);
				pairedControlPoint->SetPosition(controlDifference);
			}
		}
}


void ScaleDragCursor(){
	orxFLOAT cursorZoom = zoomLevel;
	if (cursorZoom > 0){
		cursorZoom = 1 / cursorZoom;
	}
	if (cursorZoom < 0){
		cursorZoom = 1 / cursorZoom;
	}
	
	orxVECTOR zoomVector = {cursorZoom, cursorZoom, 0};
	orxObject_SetScale(mouseDragCursorObject, &zoomVector);
}




void PauseToggleCurveObjects(){
	if (orxClock_IsPaused(pstClock) ){
		return;
	}
	if (moveAlongPaused == orxTRUE){
		moveAlongPaused = orxFALSE;
		notification->DisplayBanner("curve object\n  resumed");
	} else {
		moveAlongPaused = orxTRUE;
		notification->DisplayBanner("curve object\n   paused");
	}
}

void CheckControls(){

	if (orxInput_IsActive("ZoomIn") == orxTRUE || orxInput_IsActive("MouseWheelUp") == orxTRUE){// && orxInput_HasNewStatus("ZoomIn") == orxTRUE){
		zoomLevel += ZOOM_STEPS;
		if (zoomLevel > 4.0){
			zoomLevel = 4.0;
		}
		orxCamera_SetZoom(camera, zoomLevel);
		ScaleCurvePoints();
		ScaleDragCursor();
		//orxLOG("zz %f",zoomLevel);
	}
	
	if (orxInput_IsActive("ZoomOut") == orxTRUE || orxInput_IsActive("MouseWheelDown") == orxTRUE){// && orxInput_HasNewStatus("ZoomOut") == orxTRUE){
		zoomLevel -= ZOOM_STEPS;
		if (zoomLevel < 0.3){
			zoomLevel = 0.3;
		}
		orxCamera_SetZoom(camera, zoomLevel);
		ScaleCurvePoints();
		ScaleDragCursor();
		//orxLOG("zz %f",zoomLevel);

	}
	
	if (orxInput_IsActive("ResetZoom") == orxTRUE && orxInput_HasNewStatus("ResetZoom") == orxTRUE){
		zoomLevel = 1.0;
		orxCamera_SetZoom(camera, zoomLevel);
		ScaleCurvePoints();
		ScaleDragCursor();
		notification->DisplayBanner("reset zoom");
	}

	if (orxInput_IsActive("ControlMode") == orxFALSE && orxInput_IsActive("ShiftMode") == orxFALSE &&
		orxInput_IsActive("ToggleCurveObjects") == orxTRUE && orxInput_HasNewStatus("ToggleCurveObjects") == orxTRUE
	){ //Objects Toggle
		ToggleCurveObjects();
	}
	
	if (orxInput_IsActive("PauseCurveObjects") == orxTRUE && orxInput_HasNewStatus("PauseCurveObjects") == orxTRUE){
		PauseToggleCurveObjects();
	}
	
	if (orxInput_IsActive("DecreaseCurveObjectSpeed") == orxTRUE && orxInput_HasNewStatus("DecreaseCurveObjectSpeed") == orxTRUE){
		moveAlongSpeed -= moveAlongSpeedChangeUnits;
		if (moveAlongSpeed < 0.005){
			moveAlongSpeed = 0.005;
		}
 
		orxCHAR formattedMessage[50];
		orxString_NPrint(formattedMessage, 50, "speed %f", moveAlongSpeed);
		
		notification->DisplayBanner(formattedMessage);
	}
	
	if (orxInput_IsActive("IncreaseCurveObjectSpeed") == orxTRUE && orxInput_HasNewStatus("IncreaseCurveObjectSpeed") == orxTRUE){
		moveAlongSpeed += moveAlongSpeedChangeUnits;
		if (moveAlongSpeed > 0.08){
			moveAlongSpeed = 0.08;
		}
		
		orxCHAR formattedMessage[50];
		orxString_NPrint(formattedMessage, 50, "speed %f", moveAlongSpeed);
		
		notification->DisplayBanner(formattedMessage);
	}
	
	if (orxInput_IsActive("ShiftMode") == orxTRUE && orxInput_IsActive("ControlMode") == orxFALSE &&
		orxInput_IsActive("S") == orxTRUE && orxInput_HasNewStatus("S") == orxTRUE
	){
		point *selectedPoint = GetHighlightedPoint();
		SnapPoint(selectedPoint);

	}
	
	if (orxInput_IsActive("ControlMode") == orxTRUE && orxInput_IsActive("ShiftMode") == orxFALSE &&
		orxInput_IsActive("S") == orxTRUE && orxInput_HasNewStatus("S") == orxTRUE
	){
		SaveCurveToFile();
	}
	
	if (orxInput_IsActive("ControlMode") == orxTRUE && orxInput_IsActive("ShiftMode") == orxFALSE &&
		orxInput_IsActive("OpenFile") == orxTRUE && orxInput_HasNewStatus("OpenFile") == orxTRUE
	){ //Open
		LoadCurveFromFile();
	}
	
	if (orxInput_IsActive("ControlMode") == orxTRUE && orxInput_IsActive("ShiftMode") == orxFALSE &&
		orxInput_IsActive("ChangeBackground") == orxTRUE && orxInput_HasNewStatus("ChangeBackground") == orxTRUE
	){ //Open
		OpenBackgroundFromFile();
	}
	
	if (orxInput_IsActive("ControlMode") == orxFALSE && orxInput_IsActive("ShiftMode") == orxFALSE &&
		orxInput_IsActive("ChangeBackground") == orxTRUE && orxInput_HasNewStatus("ChangeBackground") == orxTRUE
	){ //Open
		ClearBackground();
	}
	
	if (orxInput_IsActive("ControlMode") == orxTRUE && orxInput_IsActive("ShiftMode") == orxTRUE &&
		orxInput_IsActive("S") == orxTRUE && orxInput_HasNewStatus("S") == orxTRUE
	){
		SaveAsCurveToFile();
	}
	
	if (orxInput_IsActive("Help") == orxTRUE && orxInput_HasNewStatus("Help") == orxTRUE){
		ToggleHelp();
	}
	
	if (orxInput_IsActive("ShiftMode") == orxTRUE &&
		orxInput_IsActive("Slash") == orxTRUE && orxInput_HasNewStatus("Slash") == orxTRUE
	){
		ToggleHelp();
	}
	
	if (orxInput_IsActive("decreaseObjectDistance") == orxTRUE && orxInput_HasNewStatus("decreaseObjectDistance") == orxTRUE){
		moveAlongSpread -= 0.01;
		if (moveAlongSpread < 0){
			moveAlongSpread = 0;
		}
		
		ResetMoveAlongsToZero();
		
		orxCHAR formattedMessage[30];
		orxString_NPrint(formattedMessage, 30, "wave distance:\n%f", moveAlongSpread);
		
		notification->DisplayBanner(formattedMessage);
	}
	
	if (orxInput_IsActive("increaseObjectDistance") == orxTRUE && orxInput_HasNewStatus("increaseObjectDistance") == orxTRUE){
		moveAlongSpread += 0.01;
		if (moveAlongSpread > 0.1){
			moveAlongSpread = 0.1;
		}
		
		ResetMoveAlongsToZero();
		
		orxCHAR formattedMessage[30];
		orxString_NPrint(formattedMessage, 30, "wave distance:\n%f", moveAlongSpread);
		
		notification->DisplayBanner(formattedMessage);
	}
	
	if (orxInput_IsActive("Grab") == orxTRUE && orxInput_HasNewStatus("Grab") == orxTRUE){
		grabMode = orxTRUE;

	}
	
	if (orxInput_IsActive("Grab") == orxFALSE && orxInput_HasNewStatus("Grab") == orxTRUE){
		grabMode = orxFALSE;
	}
	
	if (orxInput_IsActive("FlipX") == orxFALSE && orxInput_HasNewStatus("FlipX") == orxTRUE){
		FlipOnX();
	}
	
	if (orxInput_IsActive("FlipY") == orxFALSE && orxInput_HasNewStatus("FlipY") == orxTRUE){
		FlipOnY();
	}
}

/*  This routine takes the opposite cp, and aligns it with the point and control point
	while maintaining the opposite cp's origin distance. */
orxVECTOR GetRealignedOppositeControlPointPosition(orxVECTOR p, orxVECTOR cp, orxVECTOR oppositeCp){
	orxVECTOR distance = orxVECTOR_0;
	orxVECTOR oppositeDistance = orxVECTOR_0;

	orxVECTOR direction = orxVECTOR_0;
	orxVECTOR thisCurvePoint = p;
	orxVECTOR thisControlPoint = cp;
	orxVECTOR oppositeControlPoint = oppositeCp;
	
	orxVECTOR newControlPointPosition = oppositeControlPoint;

	orxFLOAT distance2to3 = orxVector_GetDistance(&thisCurvePoint, &oppositeControlPoint);
	orxVector_Sub(&direction, &thisCurvePoint, &thisControlPoint);
	orxVector_Normalize(&direction, &direction);
	orxVector_Mulf(&newControlPointPosition, &direction, distance2to3);
	orxVector_Add(&newControlPointPosition, &newControlPointPosition, &thisCurvePoint);

	return newControlPointPosition;
}

void SetHeaderCoordinates(orxVECTOR position){
	orxCHAR formattedCoordinates[50];
	orxString_NPrint(formattedCoordinates, 50, "x: %f y: %f", position.fX, position.fY);
	//orxLOG("x: %f y: %f", position.fX, position.fY );
	orxObject_SetTextString(pointPositionObject, formattedCoordinates);
}

void ClearHeaderCoordinates(){
	orxObject_SetTextString(pointPositionObject, "x: --- y: ---");
}

void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
	
	orxVECTOR delta = orxVECTOR_0;
	orxMouse_GetMoveDelta(&delta);
	
	CheckControls();
	
	orxVECTOR mousePosition = orxVECTOR_0;
	orxVECTOR localMousePosition = orxVECTOR_0;
	orxMouse_GetPosition(&localMousePosition);

	orxRender_GetWorldPosition(&localMousePosition, orxNULL, &mousePosition);
	
	
	orxVECTOR objectBeingPressedPosition = orxVECTOR_0;
	
	orxOBJECT *objectUnderMouse = orxObject_Pick(&mousePosition, orxCamera_GetGroupID(camera, 0));
	
	// if not pressed, hightlighting and selection is possible
	if (orxInput_IsActive("LeftClick") == orxFALSE){
		isMouseOffsetSampled = orxFALSE;
		
		if (objectUnderMouse)
		{
			point *p = (point *)orxObject_GetUserData( objectUnderMouse  );

			if (OrxStringEqual(orxObject_GetName(objectUnderMouse), "PointObject")){
				p->Highlight();
				SetHeaderCoordinates(p->GetPosition());
			}
			if (OrxStringEqual(orxObject_GetName(objectUnderMouse), "ControlObject")){
				p->Highlight();
				SetHeaderCoordinates(p->GetPosition());
			}
		} else { //if no object in objectUnderMouse
			//traverse all points and reset colour.
			rootCurveSeg->RestoreColour(); //executes too much. Needs a mouse off event
			ClearHeaderCoordinates();
		}
	}
	
	//capture the distance only if clicked and over it and not already defined. sample the distance from mouse to 
	if (orxInput_IsActive("LeftClick") == orxTRUE && orxInput_HasNewStatus("LeftClick") == orxTRUE){
		
		if (objectUnderMouse && objectBeingPressed == orxNULL && isMouseOffsetSampled == orxFALSE){
			
			//orxLOG("sample distance");
			objectBeingPressed = objectUnderMouse;
			orxObject_GetPosition(objectBeingPressed, &objectBeingPressedPosition);
			
			orxRender_GetScreenPosition(&objectBeingPressedPosition, orxNULL, &objectBeingPressedPosition);
			
			orxVector_Sub(&mouseOffset, &localMousePosition, &objectBeingPressedPosition);
			isMouseOffsetSampled == orxTRUE;
		} 
		
	}
	
	

	
	if (orxInput_IsActive("Delete") == orxTRUE && orxInput_HasNewStatus("Delete") == orxTRUE
	){
		if (GetSegmentsInCurveCount() > 2){
			//orxLOG("DELETE");
			moveAlongPositionIndex = 0;
			moveAlongSegment = rootCurveSeg;
			
			point *p = GetHighlightedPoint();
			if (p != NULL){
				//orxClock_Pause(pstClock);
				
				curvesegment *owningSegment = p->owningSegment;
				curvesegment *parentSegment = GetParentSegment(p->owningSegment);
				
				if (owningSegment == GetFinalSegment()){
					if (parentSegment != NULL){
						curvesegment *grandParentSegment = GetParentSegment(parentSegment);
						if (grandParentSegment != NULL){
							grandParentSegment->nextSegment = NULL;
							grandParentSegment->pairedSegment = NULL;
							parentSegment->DeleteOrxObjects();				
							owningSegment->DeleteOrxObjects();
							delete parentSegment;
							delete owningSegment;
							
							//grandParentSegment->EnableSegmentCurvePoint();
						}
					}
				} else if (owningSegment == rootCurveSeg){
					if (owningSegment->nextSegment != NULL){
						curvesegment *grandChildSegment = owningSegment->nextSegment->nextSegment;
						if (grandChildSegment != NULL){
							grandChildSegment->pairedSegment = NULL;
							orxObject_Enable(grandChildSegment->curvePoint->object, orxTRUE);
							rootCurveSeg = grandChildSegment;
						}
						
						owningSegment->nextSegment->DeleteOrxObjects();				
						owningSegment->DeleteOrxObjects();
						delete owningSegment->nextSegment;
						delete owningSegment;
						moveAlongSegment = rootCurveSeg;
					}
				} else { //inbetween point, not start, not end
					curvesegment *pairedSegment = p->owningSegment->pairedSegment;
					
					curvesegment *segmentCutFrom = parentSegment;
					curvesegment *segmentToJoinTo = NULL;

					curvesegment *segmentToJoinBack = NULL;
					if (pairedSegment != NULL){
						segmentToJoinBack = owningSegment->nextSegment; //no
					}

					curvesegment *segment = rootCurveSeg;
					
					while (segment != NULL){
						if (segment->nextSegment != NULL){
							if (segment->nextSegment == segmentCutFrom){
								segmentToJoinTo = segment;
								break;
							}
						} 

						segment = segment->nextSegment;
					}
					
					if (segmentToJoinBack != NULL && segmentToJoinTo != NULL){
						segmentToJoinTo->nextSegment = segmentToJoinBack;
					}

					if (owningSegment != NULL && pairedSegment != NULL){
						pairedSegment->DeleteOrxObjects();				
						owningSegment->DeleteOrxObjects();

						delete pairedSegment;
						delete owningSegment;
						
					}
				}
				
				CorrectLastSegment();
				
				//orxClock_Unpause(pstClock);
				
			}
		}
		
	}
	
	//release when mouse up
	if (objectBeingPressed != orxNULL && orxMouse_IsButtonPressed(orxMOUSE_BUTTON_LEFT) == orxFALSE){
		objectBeingPressed = orxNULL;
		objectBeingPressedPosition = orxVECTOR_0;
	}
	
	if (orxInput_IsActive("MiddleClick") == orxFALSE){
		orxObject_Enable(mouseDragCursorObject, orxFALSE);
		orxMouse_ShowCursor(orxTRUE);
	}
	
	if (orxInput_IsActive("MiddleClick") == orxTRUE){
		orxVECTOR cameraMouseDragPosition = orxVECTOR_0;
		orxCamera_GetPosition(camera, &cameraMouseDragPosition);
		
		orxFLOAT zoomLevel = orxCamera_GetZoom(camera);
		if (zoomLevel != 0){
			zoomLevel = 1 / zoomLevel;
		}
		
		orxVector_Mulf(&delta, &delta, zoomLevel); 	
		
		orxVector_Sub(&cameraMouseDragPosition, &cameraMouseDragPosition, &delta);
		cameraMouseDragPosition.fZ = -1.0;
		
		orxCamera_SetPosition(camera, &cameraMouseDragPosition);
		
		orxVECTOR mousePosition = orxVECTOR_0;
		orxMouse_GetPosition(&mousePosition);
		orxRender_GetWorldPosition(&mousePosition, orxNULL, &mousePosition);
		mousePosition.fX -= 10;
		mousePosition.fY -= 10;
		mousePosition.fZ = 0;
		orxObject_SetPosition(mouseDragCursorObject, &mousePosition);
		orxObject_Enable(mouseDragCursorObject, orxTRUE);
		orxMouse_ShowCursor(orxFALSE);
		
	}



	//when dragging an object. The distance vector should already be known
	if (orxInput_IsActive("LeftClick") == orxTRUE){
		//orxLOG("Drag");
		if (grabMode == orxTRUE){
			MoveEntireCurveByDeltaOffset(delta);
		}
		if (objectBeingPressed != orxNULL){
			if (OrxStringEqual(orxObject_GetName(objectBeingPressed), "MouseDragCursorObject")){
				objectBeingPressed = orxNULL;
				return;
			}
			orxVECTOR objectMouseDragPosition = mousePosition;
			orxVector_Sub(&objectMouseDragPosition, &objectMouseDragPosition, &mouseOffset);
			objectMouseDragPosition.fZ = 0;
			
			point *p = (point *)orxObject_GetUserData( objectBeingPressed  );

			if (p != NULL){
				SetHeaderCoordinates(p->GetPosition());
				if (OrxStringEqual(p->GetTypeName(), "PointObject")){
					p->owningSegment->SetPointPosition(objectMouseDragPosition);
					rootCurveSeg->ClearSelectedPoints();
					p->MarkSelected();
					
					curvesegment *pairedSegment = p->owningSegment->pairedSegment;
					if (pairedSegment != NULL){
						pairedSegment->SetPointPosition(objectMouseDragPosition);
					}
				} else {
					p->owningSegment->SetControlPointPosition(objectMouseDragPosition);
					//If there is a paired control point, move it in an opposite direction
					curvesegment *pairedSegment = p->owningSegment->pairedSegment;
					if (pairedSegment != NULL){

						orxVECTOR thisCurvePoint = orxVECTOR_0;
						orxVECTOR thisControlPoint = orxVECTOR_0;
						orxVECTOR oppositeControlPoint = orxVECTOR_0;
						
						thisCurvePoint = p->owningSegment->curvePoint->GetPosition();
						thisControlPoint = p->owningSegment->controlPoint->GetPosition();
						oppositeControlPoint = pairedSegment->controlPoint->GetPosition();

						
						//For both control points moving the same.
						//orxVector_Sub(&distance, &thisCurvePoint, &thisControlPoint);
						//orxVECTOR newControlPointPosition = orxVECTOR_0;
						//orxVector_Add(&newControlPointPosition, &thisCurvePoint, &distance);
						//pairedSegment->SetControlPointPosition(newControlPointPosition);
						
						orxVECTOR newControlPointPosition = GetRealignedOppositeControlPointPosition(thisCurvePoint, thisControlPoint, oppositeControlPoint);
						
						pairedSegment->SetControlPointPosition(newControlPointPosition);
					}
				}
			}
	
		} 
	}
	
	//Add new point
	if (orxInput_IsActive("ControlMode") == orxTRUE &&
		orxInput_IsActive("LeftClick") == orxTRUE && orxInput_HasNewStatus("LeftClick") == orxTRUE
	){
		if (!objectUnderMouse){
			point *selectedPoint = GetHighlightedPoint();
			if (selectedPoint != NULL && selectedPoint->owningSegment == rootCurveSeg){
				curvesegment *newRootSegment = new curvesegment(mousePosition.fX, mousePosition.fY,
															mousePosition.fX + 50, mousePosition.fY - 50
															);
				curvesegment *adjacentSegment = newRootSegment->CreateAdjacentCurveSegmentToHead(rootCurveSeg);
				//adjacentSegment->AttachNext(rootCurveSeg);
				adjacentSegment->isDrawable = orxFALSE;
				rootCurveSeg = newRootSegment;
				rootCurveSeg->ClearSelectedPoints();
				rootCurveSeg->curvePoint->MarkSelected();
			} else {
				curvesegment *finalSegment = GetFinalSegment();
				curvesegment *finalAdjacentSegment = finalSegment->CreateAdjacentCurveSegment();
				curvesegment *newSegment = new curvesegment(mousePosition.fX, mousePosition.fY,
															mousePosition.fX + 50, mousePosition.fY - 50
															);
				finalAdjacentSegment->AttachNext(newSegment);	
				
				rootCurveSeg->ClearSelectedPoints();
				newSegment->curvePoint->MarkSelected();
			}
			
			ScaleCurvePoints();
		}
	} 

	if (orxInput_IsActive("Quit") && orxInput_HasNewStatus("Quit")){
		if (orxObject_IsEnabled(helpObject) == orxTRUE){
			orxObject_EnableRecursive(helpObject, orxFALSE);
		} else {
			if (OpenQuitDialog() == orxTRUE){
				SaveSettingsToFile();
				orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
			}
		}
	}


}

void orxFASTCALL Exit()
{
	orxLOG("=================== EXIT ================");
}


/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;
  return eResult;
}


void CreateAlongCurveObjects(){
	for (int x=0; x<8; x++){
		objectsAlongCurve[x] = orxObject_CreateFromConfig("AlongCurveObject");
		orxObject_Enable(objectsAlongCurve[x], orxFALSE);
		orxVECTOR position = orxVECTOR_0;
		orxObject_SetPosition(objectsAlongCurve[x], &position);
		//moveAlongPositionIndexes[x] = 0;
		//moveAlongSegments[x] = rootCurveSeg;
	}
}

void InitialiseMoveAlongIndexesAndSegments(){
	for (int x=0; x<8; x++){
		moveAlongPositionIndexes[x] = 0;
		moveAlongSegments[x] = rootCurveSeg;
	}
}


char * Supply(){
	static char stringz[] = "Hello";
	return stringz;
}




orxSTATUS orxFASTCALL Init()
{

	char * localString;
	localString = Supply();
	
	SetupColours();
	
	orxVIEWPORT *viewPort = orxViewport_CreateFromConfig("MainViewport");
	camera = orxViewport_GetCamera(viewPort);
	
	mouseDragCursorObject = orxObject_CreateFromConfig("MouseDragCursorObject");
	orxObject_Enable(mouseDragCursorObject, orxFALSE);
	
	headerObject = orxObject_CreateFromConfig("HeaderObject");
	pointPositionObject = orxObject_CreateFromConfig("PointPositionTextObject");
	orxObject_SetParent(pointPositionObject, headerObject);
	orxObject_SetOwner(pointPositionObject, headerObject);
	
	pstClock = orxClock_Create(orx2F(0.02f));
	
	LoadAndBuildCurveFromConfig();
	CreateAlongCurveObjects();
	InitialiseMoveAlongIndexesAndSegments();
	
	moveAlongSegment = rootCurveSeg;
	
	orxObject_CreateFromConfig("CurveLayerObject");

	orxClock_Register(pstClock, MoveAlongUpdate, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Pause(pstClock, orxTRUE);
	orxClock_Register(orxClock_Get(orxCLOCK_KZ_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	
	LoadSettingsFromFile();	
	
	notification = new banner();
	helpObject = orxObject_CreateFromConfig("HelpObject");
	orxObject_EnableRecursive(helpObject, orxFALSE);

	background = orxObject_CreateFromConfig("Background");

	orxEvent_AddHandler(orxEVENT_TYPE_RENDER, RenderEventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_VIEWPORT, ViewportEventHandler);

	
	return orxSTATUS_SUCCESS;
}


orxSTATUS orxFASTCALL Bootstrap()
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

int main(int argc, char **argv)
{
	orxConfig_SetBootstrap(Bootstrap);

	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}
