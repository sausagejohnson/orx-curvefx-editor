#ifndef POINT_H
#define POINT_H

extern orxVECTOR nullVECTOR;

class curvesegment; //forward declaration
class point
{
public:
	point(orxSTRING objectConfigName, orxFLOAT x, orxFLOAT y, curvesegment *segment);
	~point();

	void parentTo(point *anotherPoint);

	orxSTRING name; //point name used to identify
	orxOBJECT *object = orxNULL;
	curvesegment *owningSegment;
	orxCOLOR highLightColour;
	orxCOLOR pointColour;
	orxCOLOR controlPointColour;
	orxCOLOR selectedColour;
	
	orxBOOL isSelected;
	
	const orxCHAR* GetTypeName(); //PointObject or ControlObject
	orxVECTOR GetPosition();
	orxVECTOR GetScreenPosition();
	
	void SetPosition(orxVECTOR newPosition);
	void SetScale(orxVECTOR scale);
	void MarkSelected();

	void Highlight();
	void StopHighlight();
	
	void DeSelect();
	
	void DeleteOrxObject();
	
private:

};

#endif // POINT_H
