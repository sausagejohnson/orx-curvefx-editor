#ifndef BANNER_H
#define BANNER_H

class banner
{
public:
	banner();
	~banner();

	orxOBJECT *bannerObject = orxNULL;
	orxOBJECT *textObject = orxNULL;

	void DisplayBanner(orxSTRING message);

private:
	void CreateObjects();

};

#endif // BANNER_H
