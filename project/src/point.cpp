#include "orx.h"
#include "point.h"

point::point(orxSTRING objectConfigName, orxFLOAT x, orxFLOAT y, curvesegment *segment)
{
	object = orxObject_CreateFromConfig(objectConfigName);
	orxObject_SetUserData(object, this);
	
	isSelected = orxFALSE;
	
	orxVECTOR position = orxVECTOR_0;
	position.fX = x;
	position.fY = y;
	position.fZ = -1.0;
	
	orxObject_SetPosition(object, &position);

	owningSegment = segment;
	
	orxVECTOR red = {1, 0, 0};
	orxVECTOR white = {1, 1, 1};
	orxVECTOR blue = {0, 0, 1};	
	orxVECTOR purple = {1, 0, 1};
	orxVECTOR yellow = {1, 1, 0};
	
	highLightColour.vRGB = white;
	highLightColour.fAlpha = orxFLOAT_1;
	
	pointColour.vRGB = red;
	pointColour.fAlpha = orxFLOAT_1;

	controlPointColour.vRGB = blue;
	controlPointColour.fAlpha = orxFLOAT_1;
	
	selectedColour.vRGB = yellow;
	selectedColour.fAlpha = orxFLOAT_1;

	
	StopHighlight();
}

//has little use due to parent space
void point::parentTo(point *anotherPoint){
	orxObject_SetParent(object, anotherPoint->object);
}

const orxCHAR* point::GetTypeName(){
	return orxObject_GetName(object);
}

orxVECTOR point::GetPosition(){
	orxVECTOR position = orxVECTOR_0;
	orxObject_GetWorldPosition(object, &position);
	
	//orxVECTOR screenPosition = orxVECTOR_0;
	//orxRender_GetScreenPosition (&position, orxNULL, &screenPosition); 
	
	return position;//screenPosition;
}

orxVECTOR point::GetScreenPosition(){
	orxVECTOR position = GetPosition();
	orxRender_GetScreenPosition (&position, orxNULL, &position); 
	return position;
}

void point::SetPosition(orxVECTOR newPosition){
	orxObject_SetWorldPosition(object, &newPosition);
}

void point::SetScale(orxVECTOR scale){
	orxObject_SetScale(object, &scale);
}

void point::MarkSelected(){
	orxObject_SetColor(object, &selectedColour);
	isSelected = orxTRUE;
}

void point::DeSelect(){
	orxObject_SetColor(object, &pointColour);
	isSelected = orxFALSE;
}


void point::Highlight(){
	if (isSelected == orxFALSE){
		orxObject_SetColor(object, &highLightColour);		
	}
}
void point::StopHighlight(){
	if (orxString_Compare(GetTypeName(), "ControlObject") == 0){
		orxObject_SetColor(object, &controlPointColour);
	} else {
		if (isSelected == orxFALSE){
			orxObject_SetColor(object, &pointColour);
		}
	}
}

void point::DeleteOrxObject(){
	orxObject_Delete(object);
}

point::~point()
{}

