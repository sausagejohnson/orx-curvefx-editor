#ifndef EDITOR_H
#define EDITOR_H

#include "orx.h"
#include "point.h"
#include "curvesegment.h"
#include "banner.h"
#include "tinyfiledialogs.h"

orxCAMERA *camera;
orxCLOCK  *pstClock;
	
//const type, I'd like it to make this a static
orxVECTOR nullVECTOR = { -1, -1, 0 };

orxRGBA controlLineColour;
orxRGBA curveColour;
orxRGBA gridColour;
orxRGBA gridXZeroColour;
orxRGBA gridYZeroColour;

orxOBJECT *objectBeingPressed;
orxOBJECT *helpObject;
orxOBJECT *mouseDragCursorObject;
orxOBJECT *pointPositionObject;
orxOBJECT *objectsAlongCurve[8];
orxOBJECT *background;
orxOBJECT *headerObject;

orxVECTOR mouseOffset = orxVECTOR_0;
orxBOOL isMouseOffsetSampled = orxFALSE;

curvesegment *rootCurveSeg;
banner *notification;

orxVECTOR drawPoint1 = orxVECTOR_0;	
orxVECTOR drawPoint2 = orxVECTOR_0;	

/* MoveAlong Curve attributes */
curvesegment *moveAlongSegment;
curvesegment* moveAlongSegments[8];
orxFLOAT moveAlongPositionIndex = 0;
orxDOUBLE moveAlongPositionIndexes[8] = {0.0};
orxBOOL moveAlongPaused = orxFALSE;
orxDOUBLE moveAlongSpeed = 0.02;
const orxFLOAT moveAlongSpeedChangeUnits = 0.001;
orxDOUBLE moveAlongSpread = 0.05; //distance between each movealong object

/* Grid parameters */
orxFLOAT gridDistance = 40;

orxFLOAT zoomLevel = 1.0;
const orxFLOAT ZOOM_STEPS = 0.05;

orxBOOL grabMode = orxFALSE;

orxSTRING lastSavePath = "";

void SetupColours();
orxBOOL OrxStringEqual(const orxSTRING one, const orxSTRING two);
orxBOOL orxFASTCALL SaveCurveToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption);
orxBOOL orxFASTCALL SaveSettingsToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption);

void ToggleHelp();
curvesegment* GetFinalSegment();
int GetDrawableSegmentsInCurveCount();
int GetSegmentsInCurveCount();

void CorrectLastSegment();
orxBOOL OpenQuitDialog();
orxBOOL OpenSaveConfirmDialog();
const orxSTRING SaveFileDialog();

const orxSTRING OpenFileDialog();
const orxSTRING OpenImageFileDialog();

void InitialiseMoveAlongIndexesAndSegments();

void ClearEntireCurve();
curvesegment* GetParentSegment(curvesegment *childSegment);
void CreateCurve();
void MoveEntireCurveByDeltaOffset(orxVECTOR delta);
orxVECTOR GetTopLeftVectorOfTheCurve();
orxVECTOR GetBottomRightVectorOfTheCurve();
orxVECTOR GetCurveSize(orxVECTOR topLeft, orxVECTOR bottomRight);
void FlipOnX();
void FlipOnY();
void ScaleCurvePoints();
void LoadAndBuildCurveFromConfig();
orxSTRING ConvertCurveToCharArray();

void SaveSettingsToFile();
void LoadCurveFromFile();
void OpenBackgroundFromFile();
void SetBackground(orxSTRING path);
void EnableMoveAlongs(orxBOOL enable);
void ToggleCurveObjects();
void EnableCurveObjects(orxBOOL enable);
void LoadSettingsFromFile();
void SaveCurveToFile();

void SetBarToWhateverDisplayWidthCurrentlyIs();

void SaveCurveToFile();
void SaveAsCurveToFile();

point* GetHighlightedPoint();
void DrawGrid();
orxFLOAT GetRotationFromVectors(orxVECTOR vectorTwo, orxVECTOR vectorOne);
curvesegment* GetDrawableSegmentInList(int index);
curvesegment* GetSegmentInList(int index);
orxDOUBLE NormalisedValueFromRangeAndValue(orxDOUBLE min, orxDOUBLE val, orxDOUBLE max);
orxVECTOR GetPositionFromMoveAlongIndex(orxDOUBLE nomalisedValue); // 0 to 1


void MoveAlongObjects();
void ResetMoveAlongsToZero();
void orxFASTCALL MoveAlongUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pContext);
void DrawGrabBox();
void DrawSegments();

orxSTATUS orxFASTCALL RenderEventHandler(const orxEVENT *_pstEvent);
void SnapPoint(point *pointToSnap);

void ScaleDragCursor();


void PauseToggleCurveObjects();
void CheckControls();
orxVECTOR GetRealignedOppositeControlPointPosition(orxVECTOR p, orxVECTOR cp, orxVECTOR oppositeCp);
void SetHeaderCoordinates(orxVECTOR position);
void ClearHeaderCoordinates();
void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext);
void orxFASTCALL Exit();
orxSTATUS orxFASTCALL Run();
void CreateAlongCurveObjects();
orxSTATUS orxFASTCALL Init();
int main(int argc, char **argv);

#ifdef __orxMSVC__
// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
#endif // __orxMSVC__


#endif // POINT_H
