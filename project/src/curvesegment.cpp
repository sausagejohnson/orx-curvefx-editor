#include "orx.h"
#include "point.h"
#include "curvesegment.h"

curvesegment::curvesegment(orxFLOAT pointX, orxFLOAT pointY, orxFLOAT controlX, orxFLOAT controlY)
{
	curvePoint = new point("PointObject", pointX, pointY, this);
	controlPoint = new point("ControlObject", controlX, controlY, this);
	
	nextSegment = NULL;
	pairedSegment = NULL;
	isDrawable = orxTRUE; //drawable by default. False if right of a paired point
}

void curvesegment::AttachNext(curvesegment *seg){
	seg->isDrawable = orxFALSE;
	nextSegment = seg;
}

void curvesegment::SetDrawable(orxBOOL drawable){
	isDrawable = drawable;
	orxObject_Enable(curvePoint->object, drawable);
}

void curvesegment::EnableSegmentCurvePoint(){
	orxObject_Enable(curvePoint->object, orxTRUE);
}

void curvesegment::DisableSegmentCurvePoint(){
	orxObject_Enable(curvePoint->object, orxFALSE);
}

void curvesegment::SetPointPosition(orxVECTOR newPosition){
	//orxRender_GetScreenPosition(&newPosition, orxNULL, &newPosition);
	
	orxVECTOR oldPosition = orxVECTOR_0;
	oldPosition = curvePoint->GetPosition();
	//orxRender_GetScreenPosition(&oldPosition, orxNULL, &oldPosition);

	orxVECTOR difference = orxVECTOR_0;
	orxVector_Sub(&difference, &newPosition, &oldPosition);
	
	curvePoint->SetPosition(newPosition);
	
	orxVECTOR controlPointPosition = orxVECTOR_0;
	controlPointPosition = controlPoint->GetPosition();
	
	orxVector_Add(&controlPointPosition, &controlPointPosition, &difference);
	this->SetControlPointPosition(controlPointPosition);
}

void curvesegment::SetControlPointPosition(const orxVECTOR newPosition){
	controlPoint->SetPosition(newPosition);

}

/* restore all point colours here and in the other linked curvesegments */
void curvesegment::RestoreColour(){
	curvePoint->StopHighlight();
	controlPoint->StopHighlight();
	
	curvesegment *nextSeg = this->nextSegment;
	
	while (nextSeg != NULL){
		nextSeg->curvePoint->StopHighlight();
		nextSeg->controlPoint->StopHighlight();

		nextSeg = nextSeg->nextSegment;
	}
}

void curvesegment::ScalePoints(orxFLOAT zoomLevel){
	orxVECTOR zoomVector = {zoomLevel, zoomLevel, 0};
	curvePoint->SetScale(zoomVector);
	controlPoint->SetScale(zoomVector);

	curvesegment *nextSeg = this->nextSegment;
	
	while (nextSeg != NULL){
		nextSeg->curvePoint->SetScale(zoomVector);
		nextSeg->controlPoint->SetScale(zoomVector);

		nextSeg = nextSeg->nextSegment;
	}
}

void curvesegment::ClearSelectedPoints(){
	
	curvePoint->DeSelect();

	curvesegment *nextSeg = this->nextSegment;
	
	while (nextSeg != NULL){
		nextSeg->curvePoint->DeSelect();

		nextSeg = nextSeg->nextSegment;
	}
}

curvesegment* curvesegment::CreateAdjacentCurveSegmentToHead(curvesegment *headSegmentToJoinTo){
	orxVECTOR distance = orxVECTOR_0;
	orxVECTOR headSegmentCurvePoint = orxVECTOR_0;
	orxVECTOR headSegmentControlPoint = orxVECTOR_0;
	
	headSegmentCurvePoint = headSegmentToJoinTo->curvePoint->GetPosition();
	headSegmentControlPoint = headSegmentToJoinTo->controlPoint->GetPosition();

	orxVector_Sub(&distance, &headSegmentCurvePoint, &headSegmentControlPoint);
	
	orxVECTOR newControlPointPosition = orxVECTOR_0;
	orxVector_Add(&newControlPointPosition, &headSegmentCurvePoint, &distance);
	
	curvesegment *nextAjacentSegment = new curvesegment(headSegmentCurvePoint.fX, headSegmentCurvePoint.fY,
		newControlPointPosition.fX, newControlPointPosition.fY
	);
	
	orxObject_Enable(nextAjacentSegment->curvePoint->object, orxFALSE);
	
	nextSegment = nextAjacentSegment;
	//pairedSegment = nextAjacentSegment;
	nextAjacentSegment->PairWithSegment(headSegmentToJoinTo);
	nextAjacentSegment->nextSegment = headSegmentToJoinTo;
	headSegmentToJoinTo->PairWithSegment(nextAjacentSegment);

	return nextAjacentSegment;
}

/* Create a curve segment that is essentially a copy of the current
 * point, and the control point is calculated from the opposite side of this
 * control point. Finally, it is added to as the nextSegment to this.
 * */
curvesegment* curvesegment::CreateAdjacentCurveSegment(){
	orxVECTOR distance = orxVECTOR_0;
	orxVECTOR thisCurvePoint = orxVECTOR_0;
	orxVECTOR thisControlPoint = orxVECTOR_0;
	
	thisCurvePoint = curvePoint->GetPosition();
	thisControlPoint = controlPoint->GetPosition();

	orxVector_Sub(&distance, &thisCurvePoint, &thisControlPoint);
	
	orxVECTOR newControlPointPosition = orxVECTOR_0;
	orxVector_Add(&newControlPointPosition, &thisCurvePoint, &distance);

	curvesegment *nextAjacentSegment = new curvesegment(thisCurvePoint.fX, thisCurvePoint.fY,
		newControlPointPosition.fX, newControlPointPosition.fY
	);
	
	orxObject_Enable(nextAjacentSegment->curvePoint->object, orxFALSE);
	
	nextSegment = nextAjacentSegment;
	pairedSegment = nextAjacentSegment;
	nextAjacentSegment->PairWithSegment(this);

	this->DisableSegmentCurvePoint();
	nextAjacentSegment->EnableSegmentCurvePoint();

	return nextAjacentSegment;
}

void curvesegment::PairWithSegment(curvesegment *segment){
	this->pairedSegment = segment;
}

void curvesegment::DeleteOrxObjects(){
	curvePoint->DeleteOrxObject();
	controlPoint->DeleteOrxObject();
}


curvesegment::~curvesegment()
{
	if (curvePoint){
		//orxObject_SetLifeTime(curvePoint->object, 0);
		//delete curvePoint;
	}
	if (controlPoint){
		//orxObject_SetLifeTime(controlPoint->object, 0);
		//delete controlPoint;
	}
}

