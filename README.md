# Orx Curve Editor #

This application allows you to create, load and save curve data that can be used in the Orx Portable Game Engine.

It is intended to be used to help design for any object that needs to travel along a curve, like an alien wave. Or to simply create curves for any use within a game.

![orx-curve-editor.png](https://alienabductionunit.com/images/orx-curve-editor.png)

### Downloads and Getting Started ###

* [Windows Binary Executable](https://gitlab.com/sausagejohnson/orx-curvefx-editor/-/raw/3bee6ee572b01f0f24685f4da1c7453bff4c8f45/orx-curve-editor-windows.zip) (Linux and Mac to follow)
* Unzip and run the editor.exe in the bin folder

### How do I use the editor ###

You can watch the following video guide to understand how to use the editor:

[![Orx Curve Editor on Youtube](https://i1.ytimg.com/vi/cnwQL3WXE8s/hqdefault.jpg)](https://www.youtube.com/watch?v=cnwQL3WXE8s)

All help is available by pressing the `F1` key, `H` key or the `?` key

The green and red grid lines indicate origin (0, 0).

Click and drag the Points and Control Points to shape the curve.

To delete a Point, select any along the curve with the left mouse, and press the `Delete` key.

Add a Point by selecting either the start or end Point on the curve with the left mouse. This will highlight the point. Hold `Control` and click the left mouse button on the grid to create a new Point.

You can snap a Point to the grid by selecting it with the left mouse button then pressing `Control` + `S`.

Zoom in and out using the `A` and `Z` keys or rolling the `Mouse Wheel`.

Reset the zoom back to 100% by pressing the `1` key.

Scroll the canvas view by pressing the middle mouse button and dragging the mouse.

### Previewing Object Waves ###

Test the curve by running a wave of aliens along it. This can be turned on or off with the `O` key.

Pause or resume the wave with the `P` key.

The speed of the wave can be increased and decreased with the `Left` and `Right` keys.

The distance between aliens can be increased or decreased with the `[` and `]` keys.

All curve manipulations can be performed live while the alien wave is active or paused.

### Mirroring (Flipping) and Moving Curves ###

You may have a useful curve, but want to mirror it so that aliens can also travel in waves in the opposite direction.

You can mirror the entire curve on the x-axis by pressing the `X` key, using point 0 as the pivot.

You can mirror the entire curve on the y-axis by pressing the `Y` key, using point 0 as the pivot.

If you are not happy with the position of the curve as a whole, you can drag the entire set elsewhere. Press and hold the `G` key to highlight the entire curve area. Drag the curve to a new position by clicking and dragging the left mouse button.

### Background Images ###

You can load and display a background image from your game to help design your curves. Press Control and `B` key to load in an image with the file browser. Remove the image with the `B` key.

### Loading and Saving Curves ###

Curves can be saved to a file and loaded back in for further editing.

To load a file, press `Control` + `O` to open a file dialog. Select a file containing a saved curve.

To save a file, press `Control` + `S` to save over the last loaded or save file.

To save as a new file, press `Control` + `Shift` + `S`.

### Using Curve data in your own Orx games ###

Follow this [handy guide](https://orx-project.org/wiki/en/tutorials/orxscroll/scrollobject_along_a_curve) to start using curves in your game. This article contains tips and code for using this curve data.

### Contact me ###

If you have any questions, problems, feedback or suggestions please do get in touch at sausage@waynejohnson.net. You can also get me on the [Orx Discord](https://orx-project.org/discord).
